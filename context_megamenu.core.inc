<?php

/**
 * Retrieve context_panels_layouts layout object.
 */
function get_layout($layout = NULL) {
  $layouts = context_panels_layouts_get_layouts();

  if (!empty($layout) && !empty($layouts)) {
    if (isset($layouts[$layout['layout']])) {
      return $layouts[$layout['layout']];
    }
  }
  // Fallback to default layout if provided.
  if (isset($layouts['default'])) {
    return $layouts['default'];
  }
  return FALSE;
}

/**
 * Add the layout stylesheet to the CSS.
 */
function add_layout_stylesheet($layout) {
  if (!empty($layout)) {
    if (!empty($layout['stylesheet'])) {
      $path = isset($layout['path']) ? $layout['path'] : drupal_get_path('theme', $layout['theme']);
      drupal_add_css($path . '/' . $layout['stylesheet']);
    }
  }
}

function build_megamenu(&$megamenu, $layout, $settings) {
  if (isset($layout['regions']) && isset($layout['theme_hook'])) {
    $content = array();
    foreach ($layout['regions'] as $region) {
      $blocks = context_megamenu_get_blocks_by_region($region, $settings);
      // Strip the 'context_panels_layouts_' prefix.
      $region = substr($region, 23);
      // TODO: output this as a renderable array instead of rendering.
      $content[$region] = $blocks ? drupal_render($blocks) : '';
    }
    if (!empty($content)) {
      $megamenu['content']['system_main']['context_panels_layouts'] = array(
        '#content' => $content,
        '#css_id' => 'context-panels-layouts-' . $layout['key'],
        '#theme' => $layout['theme_hook'],
      );
    }
  }
}

/**
 * Get a renderable array of a region containing all enabled blocks.
 */
function context_megamenu_get_blocks_by_region($region, $context) {
  module_load_include('module', 'block', 'block');
  $build = array();
  if ($list = _megamenu_block_list($region, $context['blocks'])) {
    $build = _block_get_renderable_array($list);
  }
  return $build;
}

/**
 * Return block list for region, but doesn't depending on context.
 * @see block_list() in context/plugins/context_reaction_block.inc
 */
function _megamenu_block_list($region, $blocks) {
  module_load_include('module', 'block', 'block');

  $info = context_megamenu_block_list_all_blocks();
  $context_blocks = array();

  if (!empty($blocks)) {
    foreach ($blocks as $context_block) {
      $bid = "{$context_block['module']}-{$context_block['delta']}";
      if (isset($info[$bid])) {
        $block = (object) array_merge((array) $info[$bid], $context_block);
        $block->title = isset($info[$block->bid]->title) ? $info[$block->bid]->title : NULL;
        $block->cache = isset($info[$block->bid]->cache) ? $info[$block->bid]->cache : DRUPAL_NO_CACHE;
        $context_blocks[$block->region][$block->bid] = $block;
      }
    }
  }

  foreach ($context_blocks as $r => $blocks) {
    $context_blocks[$r] = _block_render_blocks($blocks);
    // Sort blocks.
    uasort($context_blocks[$r], array('context_reaction_block', 'block_sort'));
  }
  return isset($context_blocks[$region]) ? $context_blocks[$region] : array();
}

/**
 * Helper function to get all blocks.
 * @return array return block list.
 */
function context_megamenu_block_list_all_blocks($reset = FALSE) {
  $all_blocks = &drupal_static(__FUNCTION__, NULL);

  if (!isset($all_blocks) || $reset) {
    module_load_include('module', 'context', 'plugins/context_reaction_block');
    $block_plugin = new context_reaction_block('block', '');
    $all_blocks = $block_plugin->get_blocks();
  }

  return isset($all_blocks) ? $all_blocks : array();
}