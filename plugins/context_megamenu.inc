<?php

/**
 * Expose menu items as a context condition for mega menu.
 */
class context_megamenu_condition extends context_condition {
  /**
   * Override of condition_values().
   */
  function condition_values() {
    if (module_exists('menu')) {
      $menu = menu_tree_all_data('main-menu');
      $options = array();
      foreach ($menu as $id => $item) {
        $prefix = str_repeat('-', $item['link']['depth']);
        $link_title = $prefix . $item['link']['link_title'];
        $options[$item['link']['uuid']] = $link_title;
      }
      array_unshift($options, "-- " . t('None') . " --");
    }
    else {
      $options = array();
    }
    return $options;
  }
  
  /**
   * Override of condition_form().
   * Use multiselect widget.
   */
  function condition_form($context) {
    $form = parent::condition_form($context);
    $form['#type'] = 'select';
    return $form;
  }

  function condition_form_submit($values) {
    $value[0] = $values;
    return $value;
  }
  
  function execute($uuid = NULL) {
    if ($this->condition_used()) {
      global $theme;
      $theme_default = variable_get('theme_default', 'garland');
      if (!empty($uuid) && $theme_default == $theme) {
        foreach ($this->get_contexts($uuid) as $context) {
          $this->condition_met($context, $uuid);
        }
      }
    }
  }
}